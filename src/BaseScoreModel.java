
public abstract class BaseScoreModel {
	
	public abstract double getScore(FamilyMutation m);
}