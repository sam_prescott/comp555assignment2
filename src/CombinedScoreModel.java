import java.util.ArrayList;

/**
 * A subclass of BaseScoreModel which combines scores from multiple models.
 * @author Michael Fowlie
 *
 */
public class CombinedScoreModel extends BaseScoreModel {
	private ArrayList<BaseScoreModel> children;
	
	public CombinedScoreModel()
	{
		children = new ArrayList<BaseScoreModel>();
	}
	
	public void add(BaseScoreModel child) {
		children.add(child);
	}
	
	@Override
	public double getScore(FamilyMutation m) {
		double x = 1.0;
		
		for (int i = 0; i < children.size(); i++) {
			x *= children.get(i).getScore(m);
		}
		
		return x;
	}

}
