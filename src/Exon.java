
/**
 * Represents a single line from the exon file.
 * @author 
 *
 */
public class Exon {
	public String _chr;
	public int _start1, _end1, _start2, _end2;
	
	public Exon(String chromosome, int start1, int end1, int start2, int end2){
		_chr = chromosome;
		_start1 = start1;
		_end1 = end1;
		_start2 = start2;
		_end2 = end2;
	}
}
