import java.io.*;

/**
 * Parser for the exon file.
 * @author 
 */
public class ExonParser {
	private BufferedReader _in;
	private String _filename;
	private File _file;

	public ExonParser(String filename) throws FileNotFoundException {
		_filename = filename;
		_file = new File(filename);
		_in = new BufferedReader(new FileReader(_file));
	}

	public Exon readObject() {
		String line = null;
		String[] parts;
		
		try {
			line = _in.readLine();	
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
		
		Exon exon = null;

		if (line != null) {
			try {
				parts = line.split("\t");
				exon = new Exon(parts[2], Integer.parseInt(parts[4]), Integer.parseInt(parts[5]), Integer.parseInt(parts[6]), Integer.parseInt(parts[7]));
			} catch(NumberFormatException nfe) {
				System.err.println("Error parsing integers from file.");
			}
		}
		else{
			try {
				_in.close();
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}

		return exon;
	}
}
