
/**
 * Represents a single line from the family mutation file.
 * @author 
 *
 */
public class FamilyMutation {
	public String _chromosome;
	public int _position;
	public String _expected;
	public String _actual;
	public String _name;
	private String _fatherInheritance;
	private String _motherInheritance;
	private String _daughter1Inheritance;
	private String _daughter2Inheritance;
	private String _daughter3Inheritance;
	private String _son1Inheritance;
	private String _son2Inheritance;
	
	public FamilyMutation(String chromosome, int position, String name, String expected, String actual, String father, String mother, String daughter1, String daughter2, String daughter3, String son1, String son2) {
		_chromosome = chromosome;
		_position = position;
		_name = name;
		_expected = expected;
		_actual = actual;
		_fatherInheritance = father;
		_motherInheritance = mother;
		_son1Inheritance = son1;
		_son2Inheritance = son2;
		_daughter1Inheritance = daughter1;
		_daughter2Inheritance = daughter2;
		_daughter3Inheritance = daughter3;
	}
	
	public String getInheritancePattern(){
		String temp = "";
		
		if(_fatherInheritance.contains("1"))
			temp += "1";
		else
			temp += "0";
		
		if(_motherInheritance.contains("1"))
			temp += "1";
		else
			temp += "0";
		
		if(_daughter1Inheritance.contains("1"))
			temp += "1";
		else
			temp += "0";
		
		if(_daughter2Inheritance.contains("1"))
			temp += "1";
		else
			temp += "0";
		
		if(_daughter3Inheritance.contains("1"))
			temp += "1";
		else
			temp += "0";
		
		if(_son1Inheritance.contains("1"))
			temp += "1";
		else
			temp += "0";
		
		if(_son2Inheritance.contains("1"))
			temp += "1";
		else
			temp += "0";
		
		return temp;
	}
}
