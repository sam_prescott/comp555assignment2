import java.io.*;
import java.util.zip.GZIPInputStream;

/**
 * Parser for the family mutation file.
 * @author 
 *
 */
public class FamilyMutationParser {
	public static String splitCharacter = "\\t"; //assumes all information is split on tabs
	public static String encoding = "UTF-8";
	private InputStream _fileStream;
	private InputStream _gzipStream;
	private Reader _decoder;
	private BufferedReader _buffered;

	public FamilyMutationParser(String filename) throws FileNotFoundException {
		try {
			_fileStream = new FileInputStream(filename);
			_gzipStream = new GZIPInputStream(_fileStream);
			_decoder = new InputStreamReader(_gzipStream, encoding);
			_buffered = new BufferedReader(_decoder);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public FamilyMutation readObject() {
		String line;
		String[] splits;
		try {
			line = _buffered.readLine();			
		} catch (IOException ex) {
			line = null;
			splits = null;
		}

		FamilyMutation m = null;

		if (line != null) {
			
			while(line.matches("#.*")) { //if the line starts with a #, ignore input.
				try {
					line = _buffered.readLine();			
				} catch (IOException ex) {
					line = null;
					splits = null;
				}
			}
			//have to scrub input
			//parse the data
			//create the mutation
			//organise
			splits = line.split(splitCharacter);
			//int chromosome, int position, String name, String expected, String actual, String Father, String Mother,  String D1, String D2, String D3, String S1, String S2
			m = new FamilyMutation(splits[0], Integer.parseInt(splits[1]), splits[2], splits[3], splits[4], 
					splits[9].substring(0, 3), splits[10].substring(0, 3), splits[11].substring(0, 3), splits[12].substring(0, 3), splits[13].substring(0, 3), splits[14].substring(0, 3), splits[15].substring(0, 3));

		} else {
			try {
				_buffered.close(); //do we have to close the rest here?
			} catch(IOException ex) {
				return null;
			}
		}

		return m;
	}
}
