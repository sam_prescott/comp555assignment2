
/**
 * Score model for FamilyPatterns.
 *
 */
public class FamilyPatternScoreModel extends BaseScoreModel {
	private String _pattern;
	
	public FamilyPatternScoreModel(String pattern) {
		_pattern = pattern;
	}

	@Override
	public double getScore(FamilyMutation m) {
		if(m.getInheritancePattern().equals(_pattern)) {
			return 1;
		}	return 0;
	}
}
