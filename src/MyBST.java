import java.util.ArrayList;


/**
 * @author Selina Gyde
 * ID: 1004500
 * @date April 2011
 */

public class MyBST {	
	
	public BSTNode root;
	
    public MyBST(){
	root = null;
    }
	
	public static void main(String[] args) {
		ArrayList<Pair> pairs = new ArrayList<Pair>();
		ArrayList<Exon> exons = new ArrayList<Exon>();
		
		for(Exon exon : exons){
			pairs.add(new Pair(exon._start1, exon._end1));
			pairs.add(new Pair(exon._start2, exon._end2));
		}
				
//		pairs.add(new Pair(10, 11));
//		pairs.add(new Pair(8, 9));
//		pairs.add(new Pair(1, 2));
//		pairs.add(new Pair(3, 4));
//		pairs.add(new Pair(12, 14));
//		pairs.add(new Pair(15, 16));
//		pairs.add(new Pair(27, 28));
//		pairs.add(new Pair(5, 6));
		
		MyBST binaryTree = new MyBST();									//declare and initialise a MyBST object
		
		int start = pairs.size()/2;
		Pair startPair = pairs.get(start);
		binaryTree.root = new BSTNode(startPair.l, startPair.h);
		
		for(Pair pair : pairs){
			binaryTree.addToTree(binaryTree.root, pair.l, pair.h);
		}
		
		binaryTree.inorderDisplay(binaryTree.root);
		
		System.out.println("7 is found : " + binaryTree.find(binaryTree.root, 7));
	}
	
	private static class Pair{
		int l;
		int h;
		
		public Pair(int low, int high){
			l = low;
			h = high;
		}
	}
	
	/**
     * The addToTree method takes a single String as a parameter and does one of two things. If the
     * String is already stored in the tree it increments the counter of the node storing that String. If
     * the String is not stored in the tree it adds it to the tree. The tree remains in sorted order after
     * this method has been called.
     *
     * @param s the String to be stored
     *
     **/
    
    public BSTNode addToTree(BSTNode r, int l, int h){
    	
    	if(r == null)												//if the node passed in is null
    		r = new BSTNode(l, h);									//create a new node with the value passed in
    	else if(h < r.getLow())										//else if the value of data is less than the root
    		r.left = addToTree(r.left, l, h);						//move left and recursively call method until a leaf is found
    	else if(l > r.getHigh())									//else if the value of data is greater than the root
    		r.right = addToTree(r.right, l, h);						//move right and recursively call method until a leaf is found
    	return r;													//return the root of the tree
   	
    }

    /**
     * The find method performs a traversal of the tree to discover if the given integer is stored in any of ranges in 
     * the tree or not. It returns true if the String is stored in the tree and false otherwise.
     *
     * @param i the integer to search for
     * @return true if i is stored in the tree, false if it is not
     *
     **/
    public Boolean find(BSTNode r, int i){
										
    	if(r == null)												//if the node is null
    		return(false);											//return false - clearly not in the tree!
    	if(i >= r.getLow() && i <= r.getHigh())						//if the node is the same as string passed
    		return(true);											//return true - data matches
    	if(i < r.getLow())											//if the passed value is less than the node
    		return(find(r.left, i));								//recursively call method on left subtree
    	else if(i > r.getHigh())									//if the passed value is greater than the node
    		return(find(r.right, i));								//recursively call method on right subtree
		return false;												//if all else fails, return false
    }


    /**
     * The inorderDisaply method performs an inorder traversal of the tree and outputs the data and the counter value for
     * each node in order.
     *
     * @param the root of the tree
     *
     **/
    public void inorderDisplay(BSTNode r){
    	
    	if(r.left != null)											//if there is a left node 
    		inorderDisplay(r.left);									//recursively call method moving left until leaf is reached
    	
    	System.out.print(r.getLow() + " - " + r.getHigh());		//print contents plus counter value
    	System.out.println("");										//print a blank line
    	if(r.right != null)											//if there is a right node
    		inorderDisplay(r.right);								//recursively call method moving right until leaf is reached
    		
    }

     
     protected static class BSTNode {

    		private int low;
    		private int high;

    		public BSTNode left;
    		public BSTNode right;

    		public BSTNode(int l, int h){
    		    low = l;
    		    high = h;
    		    left = null;
    		    right = null;
    		}


    		public int getLow(){
    		    return low;
    		}
    		
    		public int getHigh(){
    		    return high;
    		}
    	    }

}