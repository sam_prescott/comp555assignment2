
/**
 * Represents a single line from the population sample file.
 * @author 
 *
 */
public class PopulationSample {
	//TODO.
	public String _chromosome;
	public int _position;
	public String _expected;
	public String _actual;
	public String _name;
	public int _quality;
	public String _filterPass;
	public int AN;
	public int AC;
	private String anac;
	
	public PopulationSample(){
		
	}
	public PopulationSample(String chromosome, int position, String name, String expected, String actual, int quality, String filterPass, String anac) {
		_chromosome = chromosome;
		_position = position;
		_expected = expected;
		_actual = actual;
		_name = name;
		_quality = quality;
		_filterPass = filterPass;
		
		String[] splits = anac.split(";");
		AN = Integer.parseInt(splits[0].substring(3));
		AC = Integer.parseInt(splits[1].substring(3));
	}
}
