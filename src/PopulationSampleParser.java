import java.io.*;
import java.util.zip.GZIPInputStream;

/**
 * Parser for the family mutation file.
 * @author 
 *
 */
public class PopulationSampleParser {
	public static String splitCharacter = "\\t"; //assumes all information is split on tabs
	public static String encoding = "UTF-8";
	private InputStream _fileStream;
	private InputStream _gzipStream;
	private Reader _decoder;
	private BufferedReader _buffered;

	public PopulationSampleParser(String filename) throws FileNotFoundException {
		try {
			_fileStream = new FileInputStream(filename);
			_gzipStream = new GZIPInputStream(_fileStream);
			_decoder = new InputStreamReader(_gzipStream, encoding);
			_buffered = new BufferedReader(_decoder);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public PopulationSample readObject() {
		String line;
		String[] splits;
		try {
			line = _buffered.readLine();			
		} catch (IOException ex) {
			line = null;
			splits = null;
		}

		PopulationSample m = null;

		if (line != null) {
			
			while(line.matches("#.*")) { //if the line starts with a #, ignore input.
				try {
					line = _buffered.readLine();			
				} catch (IOException ex) {
					line = null;
					splits = null;
				}
			}
			//have to scrub input
			//parse the data
			//create the mutation
			//organise
			splits = line.split(splitCharacter);
			//chromosome, position, name, expected, actual, quality, 
			//m = new PopulationSample();
			m = new PopulationSample(splits[0], Integer.parseInt(splits[1]), splits[2], splits[3], splits[4], Integer.parseInt(splits[5]), splits[6], splits[7]);
		} else {
			try {
				_buffered.close(); //do we have to close the rest here?
			} catch(IOException ex) {
				return null;
			}
		}

		return m;
	}
}
