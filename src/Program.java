import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;


public class Program {
	private static FamilyMutationParser familyMutationParser;
	private static PopulationSampleParser populationSampleParser;
	private static ExonParser exonParser;

	private static ArrayList<String> familyPatterns;
	private static ExonTable exonTable;
	private static PopulationSampleTable populationSampleTable;

	private static ArrayList<FamilyMutationProcessor> processorList;
	private static BaseScoreModel baseModel;
	//m, f, s1, s2, d1, d2, d3
	private static String sickleCellAnaemia = 	"1111110";
	private static String retinitisPigmentosa = "0101010";
	private static String skeletalDysplasia = 	"0010010";
	private static String spasticParaplegia = 	"0101101";
	private static String[] list = {sickleCellAnaemia, retinitisPigmentosa, skeletalDysplasia, spasticParaplegia};
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Check arguments.
		//if (args.length != 4) {
		//	showUsage();
		//	return;
		//}

		//String familyFilename = args[0]; //family.vcf.gz
		String familyFilename = "family.vcf.gz";
		//String populationFilename = args[1]; //All_merged.vcf.gz
		String populationFilename = "All_merged.vcf.gz";
		//String exonsFilename = args[2]; //exons.txt
		String exonsFilename = "exons.txt";

		//Create parsers.
		try {
			familyMutationParser = new FamilyMutationParser(familyFilename);
			populationSampleParser = new PopulationSampleParser(populationFilename);
			//exonParser = new ExonParser(exonsFilename);
		}
		catch (FileNotFoundException e) {
			System.err.println("File not found!\nExiting...");
			System.exit(-1);
			return;
		}

		loadFamilyPatterns();
		//loadExons();
		loadPopulationSample();

		createBaseModels();
		loadProcessors();

		process();
	}

	public static void showUsage()
	{
		System.out.println("COMP555.A2 family.vcf.gz All_merged.vcf.gz exons.txt familypattern.txt");
	}

	/**
	 * Loads the family patterns in to memory.
	 */
	private static void loadFamilyPatterns()
	{
		familyPatterns = new ArrayList<String>();
		for( int i = 0; i < list.length; i++ ) {
			familyPatterns.add(list[i]);
		}
	}

	/**
	 * Loads the exons into memory.
	 */
	private static void loadExons()
	{
		exonTable = new ExonTable();

		while(true)
		{
			Exon e = exonParser.readObject();

			if (e != null) {
				exonTable.appendExon(e);
			} else {
				break;
			}
		}		
	}

	/**
	 * Loads the population sample into memory.
	 */
	private static void loadPopulationSample()
	{
		populationSampleTable = new PopulationSampleTable();

		while(true)
		{
			PopulationSample s = populationSampleParser.readObject();

			if (s != null) {
				populationSampleTable.appendPopulationSample(s);
			} else {
				break;
			}
		}			
	}

	/**
	 * Creates the base models used for each family pattern.
	 */
	private static void createBaseModels() {
		CombinedScoreModel combinedModel = new CombinedScoreModel();

		BaseScoreModel populationSampleModel = new PopulationSampleScoreModel(populationSampleTable);
		BaseScoreModel exonModel = exonModel = new ExonScoreModel(exonTable);

		combinedModel.add(populationSampleModel);
		combinedModel.add(exonModel);

		baseModel = combinedModel;
	}

	/**
	 * Creates models used for specific patterns, combines them with the base models, and creates a processor.
	 */
	private static void loadProcessors() {
		processorList = new ArrayList<FamilyMutationProcessor>();

		for (int i = 0; i < familyPatterns.size(); i++) {
			String p = familyPatterns.get(i);

			CombinedScoreModel localModel = new CombinedScoreModel();

			localModel.add(baseModel);
			localModel.add(new FamilyPatternScoreModel(p));

			FamilyMutationProcessor processor = new FamilyMutationProcessor(localModel);
			processorList.add(processor);
		}
	}

	private static HashMap<FamilyMutation, String> sickleCell = new HashMap<FamilyMutation, String>();
	private static HashMap<FamilyMutation, String> retitintis = new HashMap<FamilyMutation, String>();
	private static HashMap<FamilyMutation, String> skeletalDys = new HashMap<FamilyMutation, String>();
	private static HashMap<FamilyMutation, String> spasticPara = new HashMap<FamilyMutation, String>();
	/**
	 * Reads all instances of FamilyMutation from FamilyMutationParser and sends to all processors. 
	 */
	private static void process()
	{
		int testCounter = 0;
		while(true)
		{
			testCounter ++;
			FamilyMutation m = familyMutationParser.readObject();

			if (m != null) {
				if(m.getInheritancePattern().equals(list[0])) {
					sickleCell.put(m, sickleCellAnaemia);
				} else if (m.getInheritancePattern().equals(list[1])) {
					retitintis.put(m, retinitisPigmentosa);
				} else if (m.getInheritancePattern().equals(list[2])) {
					skeletalDys.put(m, skeletalDysplasia);
				} else if (m.getInheritancePattern().equals(list[3])) {
					spasticPara.put(m, spasticParaplegia);
				}
				
			} else {
				break;
			}
		}			
	}
}
